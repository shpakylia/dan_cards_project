import {doctors} from "./doctors.js";

export class Dashboard {
    constructor(api, visitsStorage) {
        this._api = api;
        this._visitsStorage = visitsStorage;
        this._dashboard = document.querySelector('.dashboard');
        this._modal = new VisitForm(api, visitsStorage, this);
        this._zindex = 2;
        let createBtn = document.querySelector('.create-btn');
        createBtn.onclick = () => {
            this._modal.show();
        };
    }

    appendCard(visitObj) {
        if (!this._dashboard.querySelector('div')) {
            this._dashboard.innerHTML = '';
        }
        const card = this._createNewCard(visitObj);
        this._dashboard.append(card);
    };

    _createNewCard(visitObj) {
        let card = document.createElement('div');
        card.dataset.id = visitObj.id;
        card.className = 'visit-card';
        card.innerHTML = `
        <span class="fa fa-times btn-close"></span>
        ${visitObj.renderMainInfo()}
        <div class="details hide">
            ${visitObj.renderDetails()}
        </div>
        <button class="btn-detail">Детали</button>
        `;
        card.querySelector('.btn-detail').onclick = (e) => {
            const details = card.querySelector('.details');
            details.classList.toggle('hide');
            if (details.classList.contains('hide')) {
                e.currentTarget.innerHTML = 'Детали';
            } else {
                e.currentTarget.innerHTML = 'Спрятать детали';
            }
        };

        card.querySelector('.btn-close').onclick = async () => {
            if (confirm('Вы точно хотите удалить карточку?'))
                await this._removeCard(card);
        };

        card.onmousedown = (event) => {
            let cardCoord = card.getBoundingClientRect();
            let dashboardCoord = this._dashboard.getBoundingClientRect();
            let shiftX = event.clientX - cardCoord.left;
            let shiftY = event.clientY - cardCoord.top;

            card.style.position = 'absolute';
            card.style.zIndex = this._zindex++;

            function moveAt(x, y) {
                let newL = x - shiftX - dashboardCoord.left;
                let newT = y - shiftY - dashboardCoord.top;

                if (newL < 0) {
                    newL = 0;
                }
                if (newT < 0) {
                    newT = 0;
                }
                if (newL + cardCoord.width > dashboardCoord.width) {
                    newL = dashboardCoord.width - cardCoord.width;
                }
                if (newT + cardCoord.height > dashboardCoord.height) {
                    newT = dashboardCoord.height - cardCoord.height;
                }

                card.style.left = newL + 'px';
                card.style.top = newT + 'px';
            }

            function onMouseMove(e) {
                moveAt(e.pageX, e.pageY);
            }

            document.addEventListener('mousemove', onMouseMove);

            card.onmouseup = function () {
                document.removeEventListener('mousemove', onMouseMove);
                card.onmouseup = null;
            };

            card.onmouseleave = function () {
                document.removeEventListener('mousemove', onMouseMove);
                card.onmouseleave = null;
            }
        };

        card.ondragstart = function () {
            return false;
        };

        return card;
    }


    async _removeCard(card) {
        await this._api.deleteVisit(card.dataset.id);
        this._visitsStorage.removeVisitItemById(card.dataset.id);
        card.remove();
    }

    appendCards(cards) {
        if (Object.keys(cards).length > 0) {
            this._dashboard.innerHTML = '';
            for (let cardId in cards) {
                this.appendCard(cards[cardId]);
            }
        }
    };
}

class VisitForm {
    constructor(api, visitsStorage, dashboard) {
        this._api = api;
        this._visitsStorage = visitsStorage;
        this._dashboard = dashboard;
        this._modal = document.querySelector('.modal');
        let closeBtn = this._modal.querySelector('.close');
        this._modal.addEventListener('mouseup', e => {
            if (e.target === this._modal || e.target === closeBtn)
                this.hide();
        });
        this._doctorsArr = doctors;
        this._form = this._modal.querySelector('form');
    }

    createForm() {
        //create label for select with doctors
        let selectionLabel = document.createElement('label');
        selectionLabel.textContent = "Врач";
        selectionLabel.classList.add('doctor-label');

        //create select with doctors
        let select = document.createElement('select');
        select.name = 'doctor';
        select.classList.add('input-form');
        for (let name in this._doctorsArr) {
            select.innerHTML += `<option value="${name}">${this._doctorsArr[name].title}</option>`
        }
        select.addEventListener('change', this.onDoctorChanged.bind(this));
        //create default input by 1st select option
        this._formControls = new Form(this._doctorsArr[Object.keys(this._doctorsArr)[0]].inputs);

        //append elems to label
        selectionLabel.append(select);
        this._form.append(selectionLabel);
        this._formControls.appendTo(this._form);
        // this._selector.querySelector('.modal-body').append(this._form);

        //add save btn
        this.createSaveBtn();

        this._form.onsubmit = async e => {
            e.preventDefault();
            const data = new FormData(this._form);
            const formData = {};
            [...data.entries()].forEach(v => {
                formData[v[0]] = v[1];
            });
            let visit = this._api.createVisitObject(formData.doctor, formData);
            visit = await this._api.createVisit(visit);
            this._visitsStorage.addVisitItem(visit);
            this._dashboard.appendCard(visit);
            this.hide();
        };
    }

    destroyForm() {
        if (this._formControls) {
            this._formControls.destroy();
            // this._form.parentNode.removeChild(this._form);
            this._form.innerHTML = '';
        }
    }

    //event when choose another doctor
    onDoctorChanged(event) {
        this._formControls.destroy();
        this._formControls = new Form(this._doctorsArr[event.target.value].inputs);
        this._formControls.appendTo(this._form);
    }

    createSaveBtn() {
        this._saveBtn = document.createElement('button');
        this._saveBtn.className = 'create-card';
        this._saveBtn.textContent = 'Создать';
        this._form.append(this._saveBtn);
    }

    show() {
        this.createForm();
        this._modal.classList.toggle('active');
    }

    hide() {
        if (this.isActive()) {
            this.destroyForm();
            this._modal.classList.toggle('active');
        }
    }

    isActive() {
        return this._modal.classList.contains('active');
    }
}

class FormControl {
    constructor(config) {
        this._config = config;
        if (['text', 'date', 'number'].includes(config.type)) {
            this.createInput();
        } else if (config.type === 'textarea') {
            this.createTextarea();
        }
    }

    createInput() {
        this._control = document.createElement('div');
        this._control.className = 'form-control';
        let attr = '';
        this._config.validators.forEach(validator => {
            if (typeof validator === 'string') {
                attr += ' validator';
            }
        });
        this._control.innerHTML = `
        <label>${this._config.title}
            <input class="input-style" type="${this._config.type}" name="${this._config.name}" ${attr}>
        </label>
        `;

    }

    createTextarea() {
        this._control = document.createElement('div');
        this._control.className = 'form-control';
        let attr = '';
        this._config.validators.forEach(validator => {
            if (typeof validator === 'string') {
                attr += ' validator';
            }
        });
        this._control.innerHTML = `
        <label >${this._config.title}
            <textarea class="input-style" name="${this._config.name}" ${attr}></textarea>
        </label>
        `;

    }

    appendTo(selector) {
        selector.append(this._control);
    }

    destroy() {
        this._control.parentNode.removeChild(this._control);
    }

}

class Form {
    constructor(controlsConfig) {
        this.controls = controlsConfig.map(c => new FormControl(c));
    }

    appendTo(selector) {
        this.controls.map(c => {
            c.appendTo(selector);
        })
    }

    destroy() {
        this.controls.map(c => {
            c.destroy();
        })
    }
}
