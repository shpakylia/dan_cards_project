import {classMap} from "./visitClasses.js";

export class Api {
    constructor(config) {
        this._config = config;
        this._token = null;
    }

    async _postRequest(url, data,) {
        if (this._token === null) {
            console.log('dont have login');
            await this.login();
        }
            console.log('token', this._token);
        return fetch(url,
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    authorization: "Bearer " + this._token
                }
            }
        ).then(response => response.json());
    }

    async _getRequest(url) {
        if (this._token === null) {
            await this.login();
        }
        return fetch(url,
            {
                method: 'GET',
                headers: {
                    authorization: "Bearer " + this._token
                }

            }
        ).then(response => response.json());
    }

    async _deleteRequest(baseUrl, id) {
        if (this._token === null) {
            await this.login();
        }
        return fetch(baseUrl + id,
            {
                method: 'DELETE',
                headers: {
                    authorization: "Bearer " + this._token
                }

            }
        ).then(response => response.json());
    }

    async login() {
        this._token = await fetch(this._config.loginUrl,
            {
                method: 'POST',
                body: JSON.stringify({email: this._config.email, password: this._config.password}),
                headers: {
                    'Content-Type': 'application/json',
                }

            }
        )
            .then(response => response.json())
            .then(response => this._token = response.token)
        ;
    }

    async createVisit(data) {
        let visitData = await this._postRequest(this._config.createVisitUrl, data);

        return this.createVisitObject(visitData.doctor, visitData);
    }

    async getVisits() {
        let visits = await this._getRequest(this._config.getVisitsUrl);
        return visits.map(v => {
            return this.createVisitObject(v.doctor, v);
        });
    }

    async deleteVisit(id) {
        return await this._deleteRequest(this._config.deleteVisitBaseUrl, id);
    }

    createVisitObject(type, data) {
        const className = classMap.get(type);
        return new className(data);
    }
}