import {Dashboard} from "./dashboard.js";
import {LocalStorage} from "./localStorage.js";
import {Api} from "./api.js";


document.addEventListener('DOMContentLoaded', ready);

async function ready() {

    const connectionConfig = {
        loginUrl: 'http://cards.danit.com.ua/login',
        createVisitUrl: 'http://cards.danit.com.ua/cards',
        getVisitsUrl: 'http://cards.danit.com.ua/cards',
        deleteVisitBaseUrl: 'http://cards.danit.com.ua/cards/',
        email: 'remont.akp.kiev@gmail.com',
        password: 'qwerty'
    };
    const api = new Api(connectionConfig);
    const visitsLocal = new LocalStorage(await api.getVisits());
    const dashboard = new Dashboard(api, visitsLocal);
    dashboard.appendCards(visitsLocal.getVisitItems());

}


//token c27192587935 remont.akp.kiev@gmail.com

