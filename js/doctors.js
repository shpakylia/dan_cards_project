export let doctors = {
        'therapist': {
        title: 'Терапевт',
        inputs: [
            {
                title: 'Цель визита',
                name: 'title',
                type: 'text',
                validators: ["required"]
            },
            {
                title: 'Дата визита',
                name: 'date',
                type: 'date',
                validators: ["required"]
            },
            {
                title: 'Возраст',
                name: 'age',
                type: 'number',
                validators: ["required"]
            },
            {
                title: 'ФИО',
                name: 'userName',
                type: 'text',
                validators: ["required"]
            },
            {
                title: 'Дополнительная информация',
                name: 'description',
                type: 'textarea',
                validators: ['maxlength="400"']
            },
        ]
    },
    "cardiolog":{
        title: "Кардиолог",
        inputs:[
        {
            title: 'Цель визита',
            name: 'title',
            type: 'text',
            validators: ["required"]
        },
        {
            title: 'Дата визита',
            name: 'date',
            type: 'date',
            validators: ["required"]
        },
        {
            title: 'Обычное давление',
            name: 'pressure',
            type: 'text',
            validators: ["required"]
        },
        {
            title: 'Индекс массы тела',
            name: 'bp',
            type: 'number',
            validators: ["required"]
        },
        {
            title: 'Заболевания сердечно-сосудистой системы',
            name: 'passDiseases',
            type: 'text',
            validators: ["required"]
        },
        {
            title: 'Возраст',
            name: 'age',
            type: 'number',
            validators: ["required"]
        },
        {
            title: 'ФИО',
            name: 'userName',
            type: 'text',
            validators: ["required"]
        },
        {
            title: 'Дополнительная информация',
            name: 'description',
            type: 'textarea',
            validators: ['maxlength="400"']
        },
        ]
    },
    "dentist": {
        title: "Стоматолог",
        inputs:[
        {
            title: 'Цель визита',
            name: 'title',
            type: 'text',
            validators: ["required"]
        },
        {
            title: 'Дата визита',
            name: 'date',
            type: 'date',
            validators: ["required"]
        },
        {
            title: 'Дата последнего посещения',
            name: 'lastDate',
            type: 'date',
            validators: ["required"]
        },
        {
            title: 'ФИО',
            name: 'userName',
            type: 'text',
            validators: ["required"]
        },
        {
            title: 'Дополнительная информация',
            name: 'description',
            type: 'textarea',
            validators: ['maxlength="400"']
        },
        ]
    }

};