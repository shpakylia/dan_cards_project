import {doctors} from "./doctors.js";

export class Visit {
    constructor({doctor, title, date, userName}){
        this.doctor = doctor;
        this.title = title;
        this.date = date;
        this.userName = userName;
    }


    renderMainInfo() {
        let doctor = doctors[this.doctor].title;
        return `
        <p>ФИО: ${this.userName}</p>
        <p>Врач: ${doctor}</p>
        `;
    }

    renderDetails() {
        return '';
    }
}

export class VisitTherapist extends Visit {
    constructor({id, doctor, title, date, userName, description, age}) {
        super({id, doctor, title, date, userName});
        this.age = age;
        this.description = description;
        this.id = id;
    }

    renderDetails() {
        return `
        <p>Возраст: ${this.age}</p>
        <p>Примечание: ${this.description}</p>
        `;
    }
}

export class VisitCardiolog extends Visit {
    constructor({id, doctor, title, date, userName, pressure, bp, passDiseases, age, description}) {
        super({id, doctor, title, date, userName});
        this.pressure = pressure;
        this.bp = bp;
        this.passDiseases = passDiseases;
        this.age = age;
        this.description = description;
        this.id = id;
    }

    renderDetails() {
        return `
        <p>Давление: ${this.pressure}</p>
        <p>ИМТ: ${this.bp}</p>
        <p>Заболевания ССС: ${this.passDiseases}</p>
        <p>Возраст: ${this.age}</p>
        <p>Примечание: ${this.description}</p>
        `;
    }
}

export class VisitDentist extends Visit {
    constructor({id, doctor, title, date, userName, description, lastDate}) {
        super({id, doctor, title, date, userName});
        this.lastDate = lastDate;
        this.description = description;
        this.id = id;
    }

    renderDetails() {
        return `
        <p>Дата последнего визита: ${this.lastDate}</p>
        <p>Примечание: ${this.description}</p>
        `;
    }
}

export const classMap = new Map();
classMap.set('therapist', VisitTherapist);
classMap.set('cardiolog', VisitCardiolog);
classMap.set('dentist', VisitDentist);