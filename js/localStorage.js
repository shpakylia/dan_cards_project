export class LocalStorage {
    constructor(visits) {
        this._visits = [];
        if (visits.length > 0) {
            visits.forEach(v => {
                this._visits[v.id] = v;
            });

            this.updateStorage();
            return;
        }

        let localVisits = localStorage.getItem('visits');
        if (localVisits) {
            localVisits = JSON.parse(localVisits);
            localVisits.forEach(v => {
                this._visits[v.id] = v;
            });
        }
    }

    addVisitItem(visit) {
        this._visits[visit.id] = visit;
        this.updateStorage();
    }

    updateStorage() {
        let visitsVal = [];

        Object.keys(this._visits).forEach(element => {
            visitsVal.push(this._visits[element]);
        });

        localStorage.setItem('visits', JSON.stringify(visitsVal));
    }

    removeVisitItemById(id) {
        if (this._visits[id]) {
            delete this._visits[id];
        }
        this.updateStorage();
    }

    getVisitItems() {
        return this._visits;
    }

    getVisitItemById(id) {
        return this._visits[id];
    }

    static removeAllVisitCards() {
        localStorage.removeItem('visits');
    }

}